//Farhan Khandaker 2135266
package linearalgebra;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class Vector3dTests {
    final double tolerance = 0.0000001;

    @Test
    public void testGetMethods(){
        Vector3d testVector = new Vector3d(2, 3, 4);
        assertEquals(2, testVector.getX(), tolerance);
        assertEquals(3, testVector.getY(), tolerance);
        assertEquals(4, testVector.getZ(), tolerance);
    }

    @Test
    public void testMagnitude(){
        Vector3d testVector = new Vector3d(2, 3, 4);
        assertEquals(Math.sqrt(29), testVector.magnitude(), tolerance); 
    }

    @Test
    public void testDotProduct(){
        Vector3d testVector = new Vector3d(2, 3, 4);
        Vector3d testVector2 = new Vector3d(5, 6, 7);
        assertEquals(56, testVector.dotProduct(testVector2), tolerance);
    }

    @Test
    public void testAdd(){
        Vector3d testVector = new Vector3d(2, 3, 4);
        Vector3d testVector2 = new Vector3d(5, 6, 7);
        Vector3d testVector3 = testVector.add(testVector2);
        assertEquals(7, testVector3.getX(), tolerance);
        assertEquals(9, testVector3.getY(), tolerance);
        assertEquals(11, testVector3.getZ(), tolerance);
    }

}
