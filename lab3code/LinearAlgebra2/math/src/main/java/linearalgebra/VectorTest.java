package linearalgebra;

public class VectorTest {
    public static void main(String[] args) {
        
        Vector3d vector = new Vector3d(1, 1, 2);
        Vector3d vector2 = new Vector3d(2, 3, 4);
        System.out.println("x: " + vector.getX() + " y: " + vector.getY() + " z: " + vector.getZ());
        System.out.println("magnitude: " + vector.magnitude());
        System.out.println("product: " + vector.dotProduct(vector2));
        Vector3d vector3 = vector.add(vector2);
        System.out.println("x: " + vector3.getX() + " y: " + vector3.getY() + " z: " + vector3.getZ());

    }
}
